package br.com.frederico_mesquita.creational.entity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import javax.json.Json;

public abstract class Membership implements IMembership {
	private String userId;
	private String userName;
	private Instant startDate;
	private Instant endDate;
	private Float discountPercentage;
	private SubscriptionType subscriptionType;

	public abstract void registerMember(String param);
		
	public void showMember() {
		System.out.println(this.toString());
	}
	
	public String toString() {
		return Json.createObjectBuilder()
			.add("userName", this.userName)
			.add("subscriptionType", this.subscriptionType.toString())
			.add("startDate", this.startDate.toString())
			.add("endDate", this.endDate.toString())
			.add("discountPercentage", this.discountPercentage).build().toString();
	}
	
	private Instant getExpirationDate(long yearsFromNow) {
		Instant expirationDate;
		
		if(yearsFromNow > 0) {
			expirationDate = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.ofHours(0)).plus(yearsFromNow, ChronoUnit.YEARS).toInstant(ZoneOffset.ofHours(0));
		} else {
			expirationDate = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.ofHours(0)).plus(1, ChronoUnit.MONTHS).toInstant(ZoneOffset.ofHours(0));
		}
		
		return expirationDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public void setEndDate(long yearsFromNow) {
		this.endDate = getExpirationDate(yearsFromNow);
	}

	public Float getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Float discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
}
