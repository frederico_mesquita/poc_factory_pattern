package br.com.frederico_mesquita.creational.entity;

public interface IMemberFactory {
	IMembership subscribe(SubscriptionType subscriptionType, String name);
}
