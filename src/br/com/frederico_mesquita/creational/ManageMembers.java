package br.com.frederico_mesquita.creational;

import java.util.ArrayList;
import java.util.List;

import br.com.frederico_mesquita.creational.control.MemberFactory;
import br.com.frederico_mesquita.creational.entity.IMembership;
import br.com.frederico_mesquita.creational.entity.SubscriptionType;

public class ManageMembers {
	public static void main(String[] args) {
		List<IMembership> lstMember = new ArrayList<>();
		
		MemberFactory memberFactory = new MemberFactory();
		
		lstMember.add(memberFactory.subscribe(SubscriptionType.ANNUAL_MEMBER, "Teddy"));
		lstMember.add(memberFactory.subscribe(SubscriptionType.TEMPORARY_MEMBER, "Robert"));
		lstMember.add(memberFactory.subscribe(SubscriptionType.LIFETIME, "Suzan"));
		
		for(IMembership member : lstMember) {
			member.showMember();
		}
	}
}
